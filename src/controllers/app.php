<?php
/**
 * Author: Everlon Passos (dev@everlon.com.br)
 * Date update: 11/08/2015 10:09:03
 */
    # Fun��es b�sicas da Aplica��o
    $base = $app['controllers_factory'];



    # Tela de entrada
    $base->get('/', function() use ($app) {
        // if(!$app['session']->get('is_user')) { return $app['twig']->render('login.twig'); }
        return $app['twig']->render('home.twig');
    })
        ->bind('home');



    $app->get('/rb', function() use($app) {
        /* Exemplo de verifica��o da DB com RedBeans ORM */
        $book = $app['db']::dispense("book");
        $book->author = "Santa Claus";
        $book->title = "Secrets of Christmas";
        return $app['db']::store( $book );
    });



    return $base;