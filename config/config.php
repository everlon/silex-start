<?php
/**
 * Author: Everlon Passos (dev@everlon.com.br)
 */

    # Iniciando SILEX
    $app = new Silex\Application();
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

/* ====================================================================================================== */
    # Session
    use Silex\Provider\SessionServiceProvider;
    $app->register(new SessionServiceProvider());
    $app->register( new Silex\Provider\UrlGeneratorServiceProvider() );

/* ====================================================================================================== */
    # HELPER
    require_once(__DIR__.'/../src/objs/Helpers.php');
    $app['helper'] = new Helper();

/* ====================================================================================================== */
    # ImageWorkshop
    use PHPImageWorkshop\ImageWorkshop;
    $app['ImageWorkshop'] = new PHPImageWorkshop\ImageWorkshop;
    //* http://phpimageworkshop.com

/* ====================================================================================================== */
    # Configurações do E-Mail
    $app->register( new Silex\Provider\SwiftmailerServiceProvider() );
    $app['swiftmailer.options'] = array(
                                    'host'       => 'mail.worium.com.br',
                                    'port'       => '25',
                                    'username'   => 'everlon@worium.com.br',
                                    'password'   => '*',
                                    'encryption' => null,
                                    'auth_mode'  => null
                                );

/* ====================================================================================================== */

    # Configurações TWIG
    $app->register(new Silex\Provider\TwigServiceProvider(),
                array(
                    'debug'     => true,
                    // 'cache' => 'cache',
                    'twig.path' => array( __DIR__.'/../src/view' )
            ));

/* ====================================================================================================== */

    # Definição das variáveis básicas
    //list($sub, $host, $ext1, $ext2) = explode(".",$_SERVER["SERVER_NAME"]);

    //define('DS',     DIRECTORY_SEPARATOR);
    //define('URL_BASE', 'http://'.$host.'.'.$ext1.'.'.$ext2);
    //define('ASSETS',   'http://'.$host.'.'.$ext1.'.'.$ext2.'/assets');

    define('URL_BASE', 'http://localhost/cemep');
    define('ASSETS',   URL_BASE.'/assets');

    $app['assets'] = array(
        'js'  => ASSETS.'/js',
        'css' => ASSETS.'/css',
        'img' => ASSETS.'/imgs',
    );

/* ====================================================================================================== */

    # Configurações do Banco de Dados
    //define('DB_HOST',     'localhost');
    //define('DB_DBNAME',   '');
    //define('DB_USER',     '');
    //define('DB_PASSWORD', '');
    //require_once(__DIR__.'/../src/lib/connect.class.php');
    //$app['db'] = new connect();

    # Configuração REDBEANPHP
    $app['db'] = $app->share(function() use ($app) {
            $mysqlHost     = "localhost";
            $mysqlUser     = "root";
            $mysqlPassword = "";

        $R = new \RedBeanPHP\Facade;
        $R::setup('sqlite:data/mydata.db'); /* com SQLITE */
        //$R::setup('mysql:host='.$mysqlHost.'; dbname=mydata', $mysqlUser, $mysqlPassword);
        $R::setAutoResolve( true );
        return $R;
    });

    # Configurações do Doctrine
    /*
    $app->register(new Silex\Provider\DoctrineServiceProvider(), array(
        'db.options' => array(
            // 'driver'   => 'pdo_mysql',
            // 'dbname'   => 'my_database_name',
            // 'host'     => 'localhost',
            // 'user'     => 'root',
            // 'password' => null
            'driver' => 'pdo_sqlite',
            'path'   => 'data/mydata.db',
        ),
        //'db.options' => array('type' => 'annotation', 'path' => 'Entity', 'namespace' => 'Entity'),
    )); */

/* ====================================================================================================== */

    # Development
    date_default_timezone_set('America/Sao_Paulo');

    ini_set('display_errors', 1);
    $app['debug']   = true;

/* ====================================================================================================== */